<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%projects}}`.
 */
class m191025_150131_create_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%projects}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'name' => $this->string()->comment('Название'),
            'price' =>$this->decimal(10,3)->comment('Стоимость'),
            'start_date' => $this->date()->comment('Дата начала'),
            'end_date' => $this->date()->comment('Дата сдачи'),
        ]);
        $this->createIndex('idx-projects-user_id', '{{%projects}}', 'user_id', false);
        $this->addForeignKey("fk-projects-user_id", "{{%projects}}", "user_id", "users", "id");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()

    {
        $this->dropForeignKey("fk-projects-user_id",'{{%projects}}');
        $this->dropTable('{{%projects}}');
    }
}
