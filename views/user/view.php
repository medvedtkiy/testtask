<?php

use app\models\User;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Users */
?>
<div class="users-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'fio',
            'email:email',
            ['attribute' => 'status',
                'attribute' => 'status',
                'value' => function ($model) {
                    return User::getStatuses()[$model->status] ? User::getStatuses()[$model->status] : 'Не определен';
                }
            ],
            ['attribute' => 'created_at',
                'format' => 'date'],
            ['attribute' => 'updated_at',
                'format' => 'date'],
        ],
    ]) ?>

</div>
