<?php

use app\models\User;
use yii\helpers\Html;
use yii\helpers\Url;

return [
    [
        'class' => 'kartik\grid\CheckboxColumn',
        'width' => '20px',
    ],
    [
        'class' => 'kartik\grid\SerialColumn',
        'width' => '30px',
    ],
        // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'id',
    // ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'username',
    ],
    [
        'class'=>'\kartik\grid\DataColumn',
        'attribute'=>'fio',
    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'auth_key',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'password_hash',
//    ],
//    [
//        'class'=>'\kartik\grid\DataColumn',
//        'attribute'=>'password_reset_token',
//    ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'email',
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'status',
         'filter'=>User::getStatuses(),
         'value'=>function($model) {
             return User::getStatuses()[$model->status] ? User::getStatuses()[$model->status] :'Не определен';
         }
     ],
     [
         'class'=>'\kartik\grid\DataColumn',
         'attribute'=>'created_at',
         'format' => 'date',
     ],
    // [
        // 'class'=>'\kartik\grid\DataColumn',
        // 'attribute'=>'updated_at',
    // ],
    [
        'class' => 'kartik\grid\ActionColumn',
        'dropdown' => false,
        'vAlign'=>'middle',
        'urlCreator' => function($action, $model, $key, $index) { 
                return Url::to([$action,'id'=>$key]);
        },
        'template'=> '{view} {update} {delete} ',
        'buttons' => [
            'delete' => function ($url, $model, $key) {     // render your custom button
                return $model->username !== 'admin' ? Html::a('<span class="glyphicon glyphicon-trash"></span>',
                    ['delete', 'id' => $model->id],['role'=>'modal-remote','title'=>'Delete',
                        'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                        'data-request-method'=>'post',
                        'data-toggle'=>'tooltip',
                        'data-confirm-title'=>'Are you sure?',
                        'data-confirm-message'=>'Are you sure want to delete this item']) : '';
            },
        ],
        'viewOptions'=>['role'=>'modal-remote','title'=>'View','data-toggle'=>'tooltip'],
        'updateOptions'=>['role'=>'modal-remote','title'=>'Update', 'data-toggle'=>'tooltip'],
        'deleteOptions'=>['role'=>'modal-remote','title'=>'Delete', 
                          'data-confirm'=>false, 'data-method'=>false,// for overide yii data api
                          'data-request-method'=>'post',
                          'data-toggle'=>'tooltip',
                          'data-confirm-title'=>'Are you sure?',
                          'data-confirm-message'=>'Are you sure want to delete this item'], 
    ],

];   